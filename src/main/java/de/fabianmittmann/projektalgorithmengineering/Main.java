package de.fabianmittmann.projektalgorithmengineering;

import de.fabianmittmann.projektalgorithmengineering.algorithm.ShortestPathAlgorithmExecutor;
import de.fabianmittmann.projektalgorithmengineering.algorithm.bellmanford.BellmanFordAlgorithm;
import de.fabianmittmann.projektalgorithmengineering.algorithm.dijkstra.DijkstraAlgorithmV2;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetEdge;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetGraph;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetNode;
import de.fabianmittmann.projektalgorithmengineering.parser.GraphMLParser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fabian Mittmann
 */
public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {
        // real world data
        // us cities
        StreetGraph miamiGraph = GraphMLParser.parse(new File("data/maps/miami-map.graphml"));
        StreetGraph miamibeachGraph = GraphMLParser.parse(new File("data/maps/miamibeach-map.graphml"));
        StreetGraph fortlauderdaleGraph = GraphMLParser.parse(new File("data/maps/fortlauderdale-map.graphml"));
        // german cities
        StreetGraph bautzenGraph = GraphMLParser.parse(new File("data/maps/bautzen-map.graphml"));
        StreetGraph leunaGraph = GraphMLParser.parse(new File("data/maps/leuna-map.graphml"));
        StreetGraph markranstaedtGraph = GraphMLParser.parse(new File("data/maps/markranstaedt-map.graphml"));
        // irish cities
        StreetGraph corkGraph = GraphMLParser.parse(new File("data/maps/cork-map.graphml"));
        StreetGraph dublinGraph = GraphMLParser.parse(new File("data/maps/dublin-map.graphml"));
        StreetGraph waterfordGraph = GraphMLParser.parse(new File("data/maps/waterford-map.graphml"));

        // execute algorithm multiple times to optimize measuring time with JIT compiler
        int i = 0;
        while (i < 10) {
            new DijkstraAlgorithmV2().execute(miamiGraph, miamiGraph.findNodeById(99219385L).orElse(null));
            new BellmanFordAlgorithm().execute(miamiGraph, miamiGraph.findNodeById(99219385L).orElse(null));
            i++;
        }

        // init algorithm executors
        ShortestPathAlgorithmExecutor dijkstraExecutor = new ShortestPathAlgorithmExecutor(new DijkstraAlgorithmV2());
        ShortestPathAlgorithmExecutor bellmanFordExecutor = new ShortestPathAlgorithmExecutor(new BellmanFordAlgorithm());

        // execute algorithms on real world data
        // 99219385 -> .................. -> 1715951908 (from west to east side of miami)
        dijkstraExecutor.execute(miamiGraph, miamiGraph.findNodeById(99219385L).orElse(null), miamiGraph.findNodeById(1715951908L).orElse(null));
        bellmanFordExecutor.execute(miamiGraph, miamiGraph.findNodeById(99219385L).orElse(null), miamiGraph.findNodeById(1715951908L).orElse(null));
        // 99072348 -> 99248064 -> 99261177
        dijkstraExecutor.execute(miamibeachGraph, miamibeachGraph.findNodeById(99072348L).orElse(null), miamibeachGraph.findNodeById(99261177L).orElse(null));
        bellmanFordExecutor.execute(miamibeachGraph, miamibeachGraph.findNodeById(99072348L).orElse(null), miamibeachGraph.findNodeById(99261177L).orElse(null));
        // 96016633 -> 96016632 -> 95963587
        dijkstraExecutor.execute(fortlauderdaleGraph, fortlauderdaleGraph.findNodeById(96016633L).orElse(null), fortlauderdaleGraph.findNodeById(95963587L).orElse(null));
        bellmanFordExecutor.execute(fortlauderdaleGraph, fortlauderdaleGraph.findNodeById(96016633L).orElse(null), fortlauderdaleGraph.findNodeById(95963587L).orElse(null));
        // 291796109 -> 291796110 -> 291794953
        dijkstraExecutor.execute(bautzenGraph, bautzenGraph.findNodeById(291796109L).orElse(null), bautzenGraph.findNodeById(291794953L).orElse(null));
        bellmanFordExecutor.execute(bautzenGraph, bautzenGraph.findNodeById(291796109L).orElse(null), bautzenGraph.findNodeById(291794953L).orElse(null));
        // 2961480325 -> 2961480326 -> 2961480322
        dijkstraExecutor.execute(leunaGraph, leunaGraph.findNodeById(2961480325L).orElse(null), leunaGraph.findNodeById(2961480322L).orElse(null));
        bellmanFordExecutor.execute(leunaGraph, leunaGraph.findNodeById(2961480325L).orElse(null), leunaGraph.findNodeById(2961480322L).orElse(null));
        // 893943701 -> 893943688 -> 893943682
        dijkstraExecutor.execute(markranstaedtGraph, markranstaedtGraph.findNodeById(893943701L).orElse(null), markranstaedtGraph.findNodeById(893943682L).orElse(null));
        bellmanFordExecutor.execute(markranstaedtGraph, markranstaedtGraph.findNodeById(893943701L).orElse(null), markranstaedtGraph.findNodeById(893943682L).orElse(null));
        // 349974993 -> 349974992 -> 413963840
        dijkstraExecutor.execute(corkGraph, corkGraph.findNodeById(349974993L).orElse(null), corkGraph.findNodeById(413963840L).orElse(null));
        bellmanFordExecutor.execute(corkGraph, corkGraph.findNodeById(349974993L).orElse(null), corkGraph.findNodeById(413963840L).orElse(null));
        // 16054088 -> 16054094 -> 2399921156
        dijkstraExecutor.execute(dublinGraph, dublinGraph.findNodeById(16054088L).orElse(null), dublinGraph.findNodeById(2399921156L).orElse(null));
        bellmanFordExecutor.execute(dublinGraph, dublinGraph.findNodeById(16054088L).orElse(null), dublinGraph.findNodeById(2399921156L).orElse(null));
        // 559789310 -> 560955545 -> 559789303
        dijkstraExecutor.execute(waterfordGraph, waterfordGraph.findNodeById(559789310L).orElse(null), waterfordGraph.findNodeById(559789303L).orElse(null));
        bellmanFordExecutor.execute(waterfordGraph, waterfordGraph.findNodeById(559789310L).orElse(null), waterfordGraph.findNodeById(559789303L).orElse(null));

        // execute algorithms on test data
        final List<StreetGraph> testGraphs = List.of(
            createTestGraph(miamiGraph, 100, 1000),
            createTestGraph(miamiGraph, 500, 5000),
            createTestGraph(miamiGraph, 1000, 10000),
            createTestGraph(miamiGraph, 3000, 15000),
            createTestGraph(miamiGraph, 5000, 20000)
        );
        // 99395803 -> 99395806
        final Long TEST_START_NODE_ID = 99395803L;
        final Long TEST_TARGET_NODE_ID = 99395806L;
        for (StreetGraph testGraph : testGraphs) {
            dijkstraExecutor.execute(testGraph, testGraph.findNodeById(TEST_START_NODE_ID).orElse(null), testGraph.findNodeById(TEST_TARGET_NODE_ID).orElse(null));
            bellmanFordExecutor.execute(testGraph, testGraph.findNodeById(TEST_START_NODE_ID).orElse(null), testGraph.findNodeById(TEST_TARGET_NODE_ID).orElse(null));
        }
    }

    private static StreetGraph createTestGraph(StreetGraph source, int maxNodes, int maxEdges) {
        List<StreetNode> nodes = new ArrayList<>();
        List<StreetEdge> edges = new ArrayList<>();

        source.getNodes()
                .stream()
                .limit(maxNodes)
                .forEach(nodes::add);
        source.getEdges()
                .stream()
                .limit(maxEdges)
                .forEach(edges::add);

        return new StreetGraph(nodes, edges);
    }

}
