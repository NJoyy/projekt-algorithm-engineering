package de.fabianmittmann.projektalgorithmengineering.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * @author Fabian Mittmann
 */
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class StreetEdge {
    private final Double distance;

    private final StreetNode sourceNode;
    private final StreetNode targetNode;
}
