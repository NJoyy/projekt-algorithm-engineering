package de.fabianmittmann.projektalgorithmengineering.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;
import java.util.Optional;

/**
 * @author Fabian Mittmann
 */
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class StreetGraph {
    private final List<StreetNode> nodes;
    private final List<StreetEdge> edges;

    public Optional<StreetNode> findNodeById(Long id) {
        return nodes
                .stream()
                .filter(node -> node.getId().equals(id))
                .findFirst();
    }
}
