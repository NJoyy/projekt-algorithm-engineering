package de.fabianmittmann.projektalgorithmengineering.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * @author Fabian Mittmann
 */
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class StreetNode implements Comparable<StreetNode> {
    private final Long id;

    @Override
    public int compareTo(StreetNode otherNode) {
        return id.compareTo(otherNode.getId());
    }
}
