package de.fabianmittmann.projektalgorithmengineering.result;

import de.fabianmittmann.projektalgorithmengineering.domain.StreetNode;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

/**
 * @author Fabian Mittmann
 */
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class ResultGraph {
    private List<StreetNode> nodes;
}
