package de.fabianmittmann.projektalgorithmengineering.algorithm;

import de.fabianmittmann.projektalgorithmengineering.domain.StreetGraph;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetNode;
import de.fabianmittmann.projektalgorithmengineering.result.ResultGraph;

/**
 * @author Fabian Mittmann
 */
public interface ShortestPathAlgorithm {
    void execute(StreetGraph sourceGraph, StreetNode startNode);

    ResultGraph getPathForTargetNode(StreetNode endNode);
}
