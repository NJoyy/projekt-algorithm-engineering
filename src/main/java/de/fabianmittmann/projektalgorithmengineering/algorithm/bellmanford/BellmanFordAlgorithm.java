package de.fabianmittmann.projektalgorithmengineering.algorithm.bellmanford;

import de.fabianmittmann.projektalgorithmengineering.algorithm.ShortestPathAlgorithm;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetEdge;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetGraph;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetNode;
import de.fabianmittmann.projektalgorithmengineering.result.ResultGraph;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @author Fabian Mittmann
 */
@Slf4j
public class BellmanFordAlgorithm implements ShortestPathAlgorithm {
    private final Map<StreetNode, Double> nodeDistances = new HashMap<>();
    private final Map<StreetNode, StreetNode> nodePredecessors = new HashMap<>();

    private final List<StreetNode> shortestPath = new LinkedList<>();
    private Long startNodeId;

    @Override
    public void execute(StreetGraph sourceGraph, StreetNode startNode) {
        nodeDistances.clear();
        nodePredecessors.clear();
        shortestPath.clear();
        this.startNodeId = startNode.getId();

        List<StreetNode> graphNodes = sourceGraph.getNodes();
        List<StreetEdge> graphEdges = sourceGraph.getEdges();

        // init
        for (StreetNode currentNode : graphNodes) {
            // init distances
            nodeDistances.put(currentNode, Double.POSITIVE_INFINITY);
            nodeDistances.replace(startNode, 0D);
            // init predecessors
            nodePredecessors.put(currentNode, null);
        }

        // execute
        for (int i = 1; i < graphNodes.size(); i++) {
            graphEdges.forEach(edge -> {
                if (nodeDistances.containsKey(edge.getSourceNode()) && nodeDistances.containsKey(edge.getTargetNode())) {
                    StreetNode sourceNode = edge.getSourceNode();
                    StreetNode targetNode = edge.getTargetNode();
                    Double sourceNodeDistance = nodeDistances.get(sourceNode);
                    Double targetNodeDistance = nodeDistances.get(targetNode);
                    Double edgeDistance = edge.getDistance();
                    if (sourceNodeDistance != Double.POSITIVE_INFINITY
                            && sourceNodeDistance + edgeDistance < targetNodeDistance) {
                        // save distance
                        nodeDistances.replace(targetNode, sourceNodeDistance + edgeDistance);
                        // save predecessor
                        nodePredecessors.put(targetNode, sourceNode);
                    }
                }
            });
        }

        // check potential of cycle of negative distances
        graphEdges.forEach(edge -> {
            if (nodeDistances.containsKey(edge.getSourceNode()) && nodeDistances.containsKey(edge.getTargetNode())) {
                Double sourceNodeDistance = nodeDistances.get(edge.getSourceNode());
                Double targetNodeDistance = nodeDistances.get(edge.getTargetNode());
                Double edgeDistance = edge.getDistance();
                if (sourceNodeDistance + edgeDistance < targetNodeDistance) {
                    log.error("There is a cycle of negative distances!");
                    throw new IllegalStateException("There is a cycle of negative distances!");
                }
            }
        });
    }

    @Override
    public ResultGraph getPathForTargetNode(StreetNode destinationNode) {
        if (shortestPath.isEmpty()) {
            shortestPath.add(destinationNode);
        }

        StreetNode predecessor = nodePredecessors.get(destinationNode);
        if (predecessor == null) {
            log.error("No Shortest Path could be found for start " +
                      "node " + new StreetNode(this.startNodeId) +
                      " and destination node " + destinationNode);
            return null;
        }
        shortestPath.add(predecessor);

        if (predecessor.getId().equals(this.startNodeId)) {
            Collections.reverse(this.shortestPath);
            return new ResultGraph(this.shortestPath);
        } else {
            return getPathForTargetNode(predecessor);
        }
    }
}
