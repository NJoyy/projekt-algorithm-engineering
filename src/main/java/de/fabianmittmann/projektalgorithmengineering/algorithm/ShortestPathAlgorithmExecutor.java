package de.fabianmittmann.projektalgorithmengineering.algorithm;

import de.fabianmittmann.projektalgorithmengineering.domain.StreetGraph;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetNode;
import de.fabianmittmann.projektalgorithmengineering.result.ResultGraph;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicLong;

import static java.util.Objects.requireNonNull;

/**
 * @author Fabian Mittmann
 */
@Slf4j
@AllArgsConstructor
public class ShortestPathAlgorithmExecutor {

    private final ShortestPathAlgorithm algorithm;

    public void execute(StreetGraph graph, StreetNode startNode, StreetNode targetNode) throws InterruptedException {
        requireNonNull(graph);
        requireNonNull(startNode);
        requireNonNull(targetNode);

        log.info("------------shortest path algorithm starting: " + algorithm.getClass().getSimpleName() + "------------");
        log.info("Number of Nodes: " + graph.getNodes().size());
        log.info("Number of Edges: " + graph.getEdges().size());
        log.info("Start node: " + startNode.getId());
        log.info("End node: " + targetNode.getId());

        // run garbage collector manually to avoid running while algorithm execution
        System.gc();

        // put algorithm processing in a thread so it can be monitored by a profiler
        AtomicLong algorithmRunningTime = new AtomicLong();
        Runnable runnable = () -> {
            long startTime = System.currentTimeMillis();
            algorithm.execute(graph, startNode);
            long endTime = System.currentTimeMillis();
            algorithmRunningTime.set(endTime - startTime);
        };
        Thread thread = new Thread(runnable);
        thread.start();
        thread.join();

        // calculate shortest path to destination node
        ResultGraph shortestPath = algorithm.getPathForTargetNode(targetNode);

        log.info("Shortest Path: " + shortestPath);
        log.info("Algorithm Running Time: " + algorithmRunningTime);
        log.info("------------shortest path algorithm terminated: " + algorithm.getClass().getSimpleName() + "------------");
        log.info("");
    }
}
