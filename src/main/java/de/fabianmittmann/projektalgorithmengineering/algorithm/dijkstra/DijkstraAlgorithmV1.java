package de.fabianmittmann.projektalgorithmengineering.algorithm.dijkstra;

import de.fabianmittmann.projektalgorithmengineering.algorithm.ShortestPathAlgorithm;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetEdge;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetGraph;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetNode;
import de.fabianmittmann.projektalgorithmengineering.result.ResultGraph;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @author Fabian Mittmann
 */
@Slf4j
public class DijkstraAlgorithmV1 implements ShortestPathAlgorithm {
    private List<StreetEdge> edges;
    private Set<StreetNode> settledNodes;
    private Set<StreetNode> unSettledNodes;
    private Map<StreetNode, StreetNode> predecessors;
    private Map<StreetNode, Double> distance;

    @Override
    public void execute(StreetGraph sourceGraph, StreetNode startNode) {
        this.edges = new ArrayList<>(sourceGraph.getEdges());

        settledNodes = new HashSet<>();
        unSettledNodes = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();
        distance.put(startNode, 0D);
        unSettledNodes.add(startNode);

        while (!unSettledNodes.isEmpty()) {
            StreetNode node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    @Override
    public ResultGraph getPathForTargetNode(StreetNode targetNode) {
        LinkedList<StreetNode> path = new LinkedList<>();
        StreetNode step = targetNode;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return new ResultGraph(path);
    }

    private void findMinimalDistances(StreetNode node) {
        List<StreetNode> adjacentNodes = getNeighbors(node);
        for (StreetNode target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }

    }

    private Double getDistance(StreetNode node, StreetNode target) {
        for (StreetEdge edge : edges) {
            if (edge.getSourceNode().equals(node)
                    && edge.getTargetNode().equals(target)) {
                return edge.getDistance();
            }
        }
        log.error("ERROR - THIS SHOULD NEVER HAPPEN");
        return null;
    }

    private List<StreetNode> getNeighbors(StreetNode node) {
        List<StreetNode> neighbors = new ArrayList<>();
        for (StreetEdge edge : edges) {
            if (edge.getSourceNode().equals(node)
                    && !isSettled(edge.getTargetNode())) {
                neighbors.add(edge.getTargetNode());
            }
        }
        return neighbors;
    }

    private StreetNode getMinimum(Set<StreetNode> nodes) {
        StreetNode minimum = null;
        for (StreetNode node : nodes) {
            if (minimum == null) {
                minimum = node;
            } else {
                if (getShortestDistance(node) < getShortestDistance(minimum)) {
                    minimum = node;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(StreetNode node) {
        return settledNodes.contains(node);
    }

    private Double getShortestDistance(StreetNode targetNode) {
        Double d = distance.get(targetNode);
        return Objects.requireNonNullElse(d, Double.MAX_VALUE);
    }
}
