package de.fabianmittmann.projektalgorithmengineering;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Fabian Mittmann
 */
public class MainLogAnalyzer {

    public static void main(String[] args) throws IOException {
        Set<Path> logFiles = new HashSet<>();

        List<Integer> miamiDijkstra = new ArrayList<>();
        List<Integer> miamibeachDijkstra = new ArrayList<>();
        List<Integer> fortlauderdaleDijkstra = new ArrayList<>();
        List<Integer> bautzenDijkstra = new ArrayList<>();
        List<Integer> leunaDijkstra = new ArrayList<>();
        List<Integer> markranDijkstra = new ArrayList<>();
        List<Integer> corkDijkstra = new ArrayList<>();
        List<Integer> dublinDijkstra = new ArrayList<>();
        List<Integer> waterfordDijkstra = new ArrayList<>();
        List<Integer> test1Dijkstra = new ArrayList<>();
        List<Integer> test2Dijkstra = new ArrayList<>();
        List<Integer> test3Dijkstra = new ArrayList<>();
        List<Integer> test4Dijkstra = new ArrayList<>();
        List<Integer> test5Dijkstra = new ArrayList<>();

        List<Integer> miamiBellmannFord = new ArrayList<>();
        List<Integer> miamibeachBellmannFord = new ArrayList<>();
        List<Integer> fortlauderdaleBellmannFord = new ArrayList<>();
        List<Integer> bautzenBellmannFord = new ArrayList<>();
        List<Integer> leunaBellmannFord = new ArrayList<>();
        List<Integer> markranBellmannFord = new ArrayList<>();
        List<Integer> corkBellmannFord = new ArrayList<>();
        List<Integer> dublinBellmannFord = new ArrayList<>();
        List<Integer> waterfordBellmannFord = new ArrayList<>();
        List<Integer> test1BellmannFord = new ArrayList<>();
        List<Integer> test2BellmannFord = new ArrayList<>();
        List<Integer> test3BellmannFord = new ArrayList<>();
        List<Integer> test4BellmannFord = new ArrayList<>();
        List<Integer> test5BellmannFord = new ArrayList<>();

        try (Stream<Path> paths = Files.walk(Paths.get("log"))) {
            paths.filter(Files::isRegularFile)
                 .forEach(logFiles::add);
        }

        logFiles.forEach(logFile -> {
            try {
                miamiDijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(6).split(" ")[3]));
                miamiBellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(15).split(" ")[3]));
                miamibeachDijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(24).split(" ")[3]));
                miamibeachBellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(33).split(" ")[3]));
                fortlauderdaleDijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(42).split(" ")[3]));
                fortlauderdaleBellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(51).split(" ")[3]));
                bautzenDijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(60).split(" ")[3]));
                bautzenBellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(69).split(" ")[3]));
                leunaDijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(78).split(" ")[3]));
                leunaBellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(87).split(" ")[3]));
                markranDijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(96).split(" ")[3]));
                markranBellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(105).split(" ")[3]));
                corkDijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(114).split(" ")[3]));
                corkBellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(123).split(" ")[3]));
                dublinDijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(132).split(" ")[3]));
                dublinBellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(141).split(" ")[3]));
                waterfordDijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(150).split(" ")[3]));
                waterfordBellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(159).split(" ")[3]));
                test1Dijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(168).split(" ")[3]));
                test1BellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(177).split(" ")[3]));
                test2Dijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(186).split(" ")[3]));
                test2BellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(195).split(" ")[3]));
                test3Dijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(204).split(" ")[3]));
                test3BellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(213).split(" ")[3]));
                test4Dijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(222).split(" ")[3]));
                test4BellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(231).split(" ")[3]));
                test5Dijkstra.add(Integer.valueOf(Files.readAllLines(logFile).get(240).split(" ")[3]));
                test5BellmannFord.add(Integer.valueOf(Files.readAllLines(logFile).get(249).split(" ")[3]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        System.out.println("Dijkstra:");
        System.out.println("Mittelwert Miami: " + getAverage(miamiDijkstra));
        System.out.println("Mittelwert Miami Beach: " + getAverage(miamibeachDijkstra));
        System.out.println("Mittelwert Fort Lauderdale: " + getAverage(fortlauderdaleDijkstra));
        System.out.println("Mittelwert Bautzen: " + getAverage(bautzenDijkstra));
        System.out.println("Mittelwert Leuna: " + getAverage(leunaDijkstra));
        System.out.println("Mittelwert Markranstaedt: " + getAverage(markranDijkstra));
        System.out.println("Mittelwert Cork: " + getAverage(corkDijkstra));
        System.out.println("Mittelwert Dublin: " + getAverage(dublinDijkstra));
        System.out.println("Mittelwert Waterford: " + getAverage(waterfordDijkstra));
        System.out.println("Mittelwert Test 1: " + getAverage(test1Dijkstra));
        System.out.println("Mittelwert Test 2: " + getAverage(test2Dijkstra));
        System.out.println("Mittelwert Test 3: " + getAverage(test3Dijkstra));
        System.out.println("Mittelwert Test 4: " + getAverage(test4Dijkstra));
        System.out.println("Mittelwert Test 5: " + getAverage(test5Dijkstra));

        System.out.println("\nBellman-Ford:");
        System.out.println("Mittelwert Miami: " + getAverage(miamiBellmannFord));
        System.out.println("Mittelwert Miami Beach: " + getAverage(miamibeachBellmannFord));
        System.out.println("Mittelwert Fort Lauderdale: " + getAverage(fortlauderdaleBellmannFord));
        System.out.println("Mittelwert Bautzen: " + getAverage(bautzenBellmannFord));
        System.out.println("Mittelwert Leuna: " + getAverage(leunaBellmannFord));
        System.out.println("Mittelwert Markranstaedt: " + getAverage(markranBellmannFord));
        System.out.println("Mittelwert Cork: " + getAverage(corkBellmannFord));
        System.out.println("Mittelwert Dublin: " + getAverage(dublinBellmannFord));
        System.out.println("Mittelwert Waterford: " + getAverage(waterfordBellmannFord));
        System.out.println("Mittelwert Test 1: " + getAverage(test1BellmannFord));
        System.out.println("Mittelwert Test 2: " + getAverage(test2BellmannFord));
        System.out.println("Mittelwert Test 3: " + getAverage(test3BellmannFord));
        System.out.println("Mittelwert Test 4: " + getAverage(test4BellmannFord));
        System.out.println("Mittelwert Test 5: " + getAverage(test5BellmannFord));
    }

    private static Integer getAverage(List<Integer> list) {
        int sum = 0;
        for (int i : list)
            sum = sum + i;
        return sum / list.size();
    }

}
