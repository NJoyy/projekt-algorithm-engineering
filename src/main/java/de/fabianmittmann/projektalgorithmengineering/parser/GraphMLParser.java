package de.fabianmittmann.projektalgorithmengineering.parser;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.util.io.graphml.GraphMLReader;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetEdge;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetGraph;
import de.fabianmittmann.projektalgorithmengineering.domain.StreetNode;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Parse street graph data from graphml file.
 *
 * @author Fabian Mittmann
 */
public class GraphMLParser {
    private GraphMLParser(){}

    public static StreetGraph parse(File file) throws IOException {
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        Graph graph = new TinkerGraph();
        GraphMLReader reader = new GraphMLReader(graph);
        reader.inputGraph(in);

        final List<StreetNode> nodes = new ArrayList<>();
        final List<StreetEdge> edges = new ArrayList<>();

        graph.getVertices().forEach(vertex ->
            nodes.add(new StreetNode(Long.valueOf((String) vertex.getId())))
        );

        graph.getEdges().forEach(edge -> {
            Double length = Double.valueOf(edge.getProperty("length"));
            Long sourceNodeId = Long.valueOf((String) edge.getVertex(Direction.OUT).getId());
            Long targetNodeId = Long.valueOf((String) edge.getVertex(Direction.IN).getId());
            edges.add(new StreetEdge(length, new StreetNode(sourceNodeId), new StreetNode(targetNodeId)));
        });

        return new StreetGraph(nodes, edges);
    }
}
