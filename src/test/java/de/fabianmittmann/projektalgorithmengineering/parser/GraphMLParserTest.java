package de.fabianmittmann.projektalgorithmengineering.parser;

import de.fabianmittmann.projektalgorithmengineering.domain.StreetGraph;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Fabian Mittmann
 */
public class GraphMLParserTest {
    @Test
    public void testParseWithTestFile() throws Exception {
        File graphmlFile = new File(Objects.requireNonNull(getClass().getClassLoader().getResource("test.graphml")).getFile());

        StreetGraph graph = GraphMLParser.parse(graphmlFile);

        assertEquals(137, graph.getNodes().size());
        assertEquals(61, graph.getEdges().size());
    }
}
